<?php

namespace App\ValueObject;

use Carbon\Carbon;
use DateTimeImmutable;
use DateTimeZone;

class OutageInfo
{

    private int $customersOutCount = 0;
    private DateTimeImmutable $updatedAt;
    private DateTimeImmutable $createdAt;
    private string $url;

    public function __construct(int $customersOutCount, string $updatedAt, string $url)
    {
        $this->customersOutCount = $customersOutCount;
        $this->updatedAt = new DateTimeImmutable($updatedAt);
        $this->createdAt = new DateTimeImmutable();
        $this->url = $url;
    }

    public function getCustomersOutCount(): ?int
    {
        return $this->customersOutCount;
    }

    public function getFormatted(): string
    {
        return sprintf("Count: %s, \nUpdated: %s, \nOn: %s, \nUrl: %s",
            $this->customersOutCount,
            Carbon::create($this->updatedAt)->diffForHumans(),
            $this->getUpdatedAt()->setTimezone(new DateTimeZone('America/Chicago'))->format(DATE_RFC850),
            $this->url
        );
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

}