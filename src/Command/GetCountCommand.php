<?php

namespace App\Command;

use App\ValueObject\OutageInfo;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Notifier\Bridge\Telegram\TelegramOptions;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(
    name: 'app:get-count',
    description: 'Add a short description for your command',
)]
class GetCountCommand extends Command
{

    private HttpClientInterface $client;
    private string $telegramToken;
    private string $telegramChatId;

    public function __construct(
        HttpClientInterface $client,
        string $telegramToken,
        string $telegramChatId
    ) {
        parent::__construct();
        $this->client = $client;
        $this->telegramToken = $telegramToken;
        $this->telegramChatId = $telegramChatId;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $outageInfo = $this->getPageContent();
        $io->info($outageInfo->getFormatted());

        $this->client->request(
            'POST',
            sprintf('https://api.telegram.org/%s/sendMessage', $this->telegramToken),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => [
                    'chat_id' => $this->telegramChatId,
                    'text' => $outageInfo->getFormatted()
                ]
            ]
        );

        return Command::SUCCESS;
    }

    private function getPageContent()
    {
        $url = 'https://poweroutage.us/area/county/1354';
        $outageCountXPath = '//html/body/div[2]/table/tr[5]/td/div[3]';
        $outageUpdatedXPath = '//html/body/div[2]/table/tr[5]/td/div[4]';
        $htmlString = $this->client->request('GET', $url)->getContent();
        $crawler = new Crawler($htmlString);
        $count = $crawler->filterXPath($outageCountXPath)->text();
        $updatedAt = $crawler->filterXPath($outageUpdatedXPath)->text();

        return new OutageInfo($count, $updatedAt, $url);
    }
}
